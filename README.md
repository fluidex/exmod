[libxm]: <https://github.com/Artefact2/libxm>

This crate provides a both safe & unsafe wrappers around the [libxm] library.

### Requirements
Building [libxm] is handled by the crate but requires GCC. In theory, building is possible with other build tools, but this hasn't been tested and therefore successfull building isn't guaranteed.

### Usage
Just add this crate to your dependencies (in the `Cargo.toml` file):
```toml
[dependencies]
exmod = "0.3"
```
And start working with it...

### Examples
See [examples](https://gitlab.com/fluidex/exmod/-/tree/master/examples) folder in repository.
