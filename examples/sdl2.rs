use exmod::Context;
use sdl2::{
    audio::{AudioCallback, AudioSpecDesired},
    init,
};
use std::sync::mpsc::{channel, Sender};

struct Callback {
    context: Context,
    sender: Sender<bool>,
}

impl AudioCallback for Callback {
    type Channel = f32;

    fn callback(&mut self, samples: &mut [Self::Channel]) {
        if self.context.current_loop_number() > 0 {
            self.sender.send(true).unwrap();
        }

        self.context.generate_samples(samples);
    }
}

fn main() {
    let (sender, receiver) = channel();

    let audio = init()
        .unwrap()
        .audio()
        .unwrap()
        .open_playback(
            None,
            &AudioSpecDesired {
                freq: None,
                channels: None,
                samples: None,
            },
            |spec| {
                let context = Context::new(include_bytes!("fall.xm"), spec.freq as u32).unwrap();

                println!("Song: \"{}\".", context.module_name().unwrap());
                println!("Tracker: \"{}\".", context.tracker_name().unwrap());

                Callback { context, sender }
            },
        )
        .unwrap();

    audio.resume();
    receiver.recv().unwrap();
}
