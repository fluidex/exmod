### Examples
To run the examples, you need to clone this repository and use the following command, where `$NAME` is the name of the example:
```sh
cargo r --example $NAME
```

⚠️ **Note:**\
SDL2 example requires SDL2 library installed on your system. For details, see [SDL2 crate requirements in its repository](https://github.com/Rust-SDL2/rust-sdl2#requirements).
___
For these examples, the awesome module ["Fall from sky"](https://modarchive.org/module.php?41177) by [Elwood](https://modarchive.org/member.php?69004) is used. All copyrights belongs to him.
