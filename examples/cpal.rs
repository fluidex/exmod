use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use exmod::Context;
use std::sync::mpsc::channel;

fn main() {
    let (sender, receiver) = channel();
    let device = cpal::default_host().default_output_device().unwrap();
    let config = device.default_output_config().unwrap().config();
    let mut context = Context::new(include_bytes!("fall.xm"), config.sample_rate.0).unwrap();

    println!("Song: \"{}\".", context.module_name().unwrap());
    println!("Tracker: \"{}\".", context.tracker_name().unwrap());

    let stream = device
        .build_output_stream(
            &config,
            move |samples: &mut [f32], _callback| {
                if context.current_loop_number() > 0 {
                    sender.send(true).unwrap();
                }

                context.generate_samples(samples);
            },
            |error| eprintln!("{}", error),
        )
        .unwrap();

    stream.play().unwrap();
    receiver.recv().unwrap();
}
