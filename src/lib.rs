//! Bindings to [`libxm`], a small XM player library.
//!
//! Start from [`Context`] struct documentation.
//!
//! [`libxm`]: https://github.com/Artefact2/libxm
//!
//! ## Feature flags:
//! - `defensive`: enables defensively checking the XM for errors/inconsistencies.
//! - `linear_interpolation`: enables linear interpolation (slightly increase CPU usage).
//! - `ramping`: enables ramping (smooth volume/panning transitions, slightly increase CPU usage).
//! - `strings`: enables module, instruments and sample names storing in context.

use std::{
    ffi::CStr,
    fmt::{self, Debug, Display, Formatter},
    mem::MaybeUninit,
    str::Utf8Error,
};

/// Raw [`libxm`] functions.
///
/// [`libxm`]: https://github.com/Artefact2/libxm
pub mod ffi;
use ffi::*;

/// Module context.
///
/// Can contain only one module and provides functions for manipulating it.
pub struct Context(ContextPointer);

unsafe impl Send for Context {}
unsafe impl Sync for Context {}

impl Debug for Context {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("Context")
            .field("module_length", &self.module_length())
            .field("patterns", &self.number_of_patterns())
            .field("instruments", &self.number_of_instruments())
            .field("channels", &self.number_of_channels())
            .field("current_loop_count", &self.current_loop_number())
            .field("speed", &self.playing_speed())
            .field("position", &self.position())
            .finish()
    }
}

impl Context {
    /// Create a context.
    ///
    /// The `module` parameter sets a contents of the module.
    /// The `rate` parameter specifies a sample rate (in Hz) with which samples will be generated.
    ///
    /// # Example
    /// ```ignore
    /// use exmod::Context;
    ///
    /// let context = Context::new(include_bytes!("module.xm"), 44100).unwrap();
    ///
    /// // And start working with it...
    /// ```
    pub fn new(module: &[u8], rate: u32) -> Result<Self, ContextError> {
        let mut pointer = MaybeUninit::uninit();
        unsafe {
            match xm_create_context_safe(
                pointer.as_mut_ptr(),
                module.as_ptr() as _,
                module.len(),
                rate,
            ) {
                0 => Ok(Self::from_ptr(pointer.assume_init())),
                #[cfg(feature = "defensive")]
                1 => Err(ContextError::ModuleNotSane),
                2 => Err(ContextError::MemoryAllocFailed),
                unknown => Err(ContextError::Unknown(unknown)),
            }
        }
    }

    /// Returns a raw pointer to the inner context.
    pub fn as_ptr(&mut self) -> ContextPointer {
        self.0
    }

    /// Creates a context directly from the raw pointer of another context.
    ///
    /// # Safety
    /// `pointer` must be previously allocated via [`new`] and then [`as_ptr`] functions or their unsafe equivalent - [`xm_create_context_safe`].
    ///
    /// [`new`]: Self::new
    /// [`as_ptr`]: Self::as_ptr
    pub unsafe fn from_ptr(pointer: ContextPointer) -> Self {
        Self(pointer)
    }

    /// Play the module and put a generated samples in the `samples` slice.
    ///
    /// Creates silence if the loop count specified by [`set_max_loop_count`] is reached. By default plays endlessly.
    ///
    /// Requires mutability because generating samples moves a playback cursor within context.
    ///
    /// [`set_max_loop_count`]: Self::set_max_loop_count
    ///
    /// # Example
    /// See [`examples`] repository folder for comprehensive examples.
    ///
    /// [`examples`]: https://gitlab.com/fluidex/exmod/-/tree/master/examples
    pub fn generate_samples(&mut self, samples: &mut [f32]) {
        unsafe { xm_generate_samples(self.0, samples.as_mut_ptr(), samples.len() / 2) }
    }

    /// Get the current number of playback loops of the module.
    ///
    /// The number will increase with each loop. When the maximum number of loops is reached,
    /// calls to [`generate_samples`] will only generate silence. You can set number of maximum
    /// loops with [`set_max_loop_count`].
    ///
    /// [`generate_samples`]: Self::generate_samples
    /// [`set_max_loop_count`]: Self::set_max_loop_count
    pub fn current_loop_number(&self) -> u8 {
        unsafe { xm_get_loop_count(self.0) }
    }

    /// Set the max number of playback loops of the module.
    ///
    /// Set to 0 to loop indefinitely.
    /// By default max loop count is 0 (i. e. infinity).
    ///
    /// See also [`current_loop_number`].
    ///
    /// [`current_loop_number`]: Self::current_loop_number
    pub fn set_max_loop_count(&mut self, count: u8) {
        unsafe { xm_set_max_loop_count(self.0, count) }
    }

    /// Get the number of module channels.
    pub fn number_of_channels(&self) -> u16 {
        unsafe { xm_get_number_of_channels(self.0) }
    }

    /// Get the module length.
    pub fn module_length(&self) -> u16 {
        unsafe { xm_get_module_length(self.0) }
    }

    /// Get the number of module patterns.
    pub fn number_of_patterns(&self) -> u16 {
        unsafe { xm_get_number_of_patterns(self.0) }
    }

    /// Get the number of module rows.
    pub fn number_of_rows(&self, pattern: u16) -> Result<u16, OutOfBoundsError> {
        if pattern > self.number_of_patterns() {
            Err(OutOfBoundsError::Patterns)
        } else {
            unsafe { Ok(xm_get_number_of_rows(self.0, pattern)) }
        }
    }

    /// Get the number of module instruments.
    pub fn number_of_instruments(&self) -> u16 {
        unsafe { xm_get_number_of_instruments(self.0) }
    }

    /// Get the number of samples of the given instrument.
    pub fn number_of_samples(&self, mut instrument: u16) -> Result<u16, OutOfBoundsError> {
        instrument += 1;
        if instrument > self.number_of_instruments() {
            Err(OutOfBoundsError::Instruments)
        } else {
            unsafe { Ok(xm_get_number_of_samples(self.0, instrument)) }
        }
    }

    /// Get the panning of the given channel.
    ///
    /// Returns a panning between 0 (L) and 1 (R).
    pub fn panning_of_channel(&self, mut channel: u16) -> Result<f32, OutOfBoundsError> {
        channel += 1;
        if channel > self.number_of_channels() {
            Err(OutOfBoundsError::Channels)
        } else {
            unsafe { Ok(xm_get_panning_of_channel(self.0, channel)) }
        }
    }

    /// Get the volume of the given channel.
    ///
    /// Returns a volume between 0 or 1.
    pub fn volume_of_channel(&self, mut channel: u16) -> Result<f32, OutOfBoundsError> {
        channel += 1;
        if channel > self.number_of_channels() {
            Err(OutOfBoundsError::Channels)
        } else {
            unsafe { Ok(xm_get_volume_of_channel(self.0, channel)) }
        }
    }

    /// Get the frequency in Hz of the given channel.
    pub fn frequency_of_channel(&self, mut channel: u16) -> Result<f32, ChannelError> {
        channel += 1;
        self.is_channel_active(channel)
            .map_err(ChannelError::OutOfBounds)
            .and_then(|active| {
                if active {
                    Ok(unsafe { xm_get_frequency_of_channel(self.0, channel) })
                } else {
                    Err(ChannelError::NotActive)
                }
            })
    }

    /// Check if the channel is active.
    pub fn is_channel_active(&self, mut channel: u16) -> Result<bool, OutOfBoundsError> {
        channel += 1;
        if channel > self.number_of_channels() {
            Err(OutOfBoundsError::Channels)
        } else {
            unsafe { Ok(xm_is_channel_active(self.0, channel)) }
        }
    }

    /// Get the latest trigger of the given instrument.
    ///
    /// Get the latest time (in number of generated samples) when
    /// the given instrument was triggered in any channel.
    pub fn latest_trigger_of_instrument(
        &self,
        mut instrument: u16,
    ) -> Result<u64, OutOfBoundsError> {
        instrument += 1;
        if instrument > self.number_of_instruments() {
            Err(OutOfBoundsError::Instruments)
        } else {
            unsafe { Ok(xm_get_latest_trigger_of_instrument(self.0, instrument)) }
        }
    }

    /// Get the latest trigger of the given sample of the given instrument.
    ///
    /// Get the latest time (in number of generated samples) when
    /// the given sample of the given instrument was triggered in any channel.
    pub fn latest_trigger_of_sample(
        &self,
        mut instrument: u16,
        sample: u16,
    ) -> Result<u64, OutOfBoundsError> {
        instrument += 1;
        if let Ok(samples) = self.number_of_samples(instrument) {
            if sample > samples {
                Err(OutOfBoundsError::Samples)
            } else {
                unsafe { Ok(xm_get_latest_trigger_of_sample(self.0, instrument, sample)) }
            }
        } else {
            Err(OutOfBoundsError::Instruments)
        }
    }

    /// Get the latest trigger of the given channel.
    ///
    /// Get the latest time (in number of generated samples) when
    /// any instrument was triggered in the given channel.
    pub fn latest_trigger_of_channel(&self, mut channel: u16) -> Result<u64, OutOfBoundsError> {
        channel += 1;
        if channel > self.number_of_channels() {
            Err(OutOfBoundsError::Channels)
        } else {
            unsafe { Ok(xm_get_latest_trigger_of_channel(self.0, channel)) }
        }
    }

    /// Get the module playing speed.
    pub fn playing_speed(&self) -> Speed {
        let (mut bpm, mut tempo) = (MaybeUninit::uninit(), MaybeUninit::uninit());
        unsafe {
            xm_get_playing_speed(self.0, bpm.as_mut_ptr(), tempo.as_mut_ptr());
            Speed {
                bpm: bpm.assume_init(),
                tempo: tempo.assume_init(),
            }
        }
    }

    /// Get the current playback position.
    pub fn position(&self) -> Position {
        let mut pattern_index = MaybeUninit::uninit();
        let mut pattern = MaybeUninit::uninit();
        let mut row = MaybeUninit::uninit();
        let mut generated_samples = MaybeUninit::uninit();
        unsafe {
            xm_get_position(
                self.0,
                pattern_index.as_mut_ptr(),
                pattern.as_mut_ptr(),
                row.as_mut_ptr(),
                generated_samples.as_mut_ptr(),
            );
            Position {
                pattern_index: pattern_index.assume_init(),
                pattern: pattern.assume_init(),
                row: row.assume_init(),
                generated_samples: generated_samples.assume_init(),
            }
        }
    }

    /// Mute the given channel.
    pub fn mute_channel(&mut self, mut channel: u16, mute: bool) -> Result<bool, OutOfBoundsError> {
        channel += 1;
        if channel > self.number_of_channels() {
            Err(OutOfBoundsError::Channels)
        } else {
            unsafe { Ok(xm_mute_channel(self.0, channel, mute)) }
        }
    }

    /// Mute the given instrument.
    pub fn mute_instrument(
        &mut self,
        mut instrument: u16,
        mute: bool,
    ) -> Result<bool, OutOfBoundsError> {
        instrument += 1;
        if instrument > self.number_of_instruments() {
            Err(OutOfBoundsError::Instruments)
        } else {
            unsafe { Ok(xm_mute_instrument(self.0, instrument, mute)) }
        }
    }

    /// Seek the playback cursor to the specified position.
    ///
    /// # Safety
    /// Seek feature is broken by design and usually leads to access violation error, use at your own risk.
    pub unsafe fn seek(&mut self, pattern_index: u8, row: u8, tick: u16) {
        xm_seek(self.0, pattern_index, row, tick)
    }
}

#[cfg(feature = "strings")]
impl Context {
    /// Get the module name.
    pub fn module_name(&self) -> Result<&str, Utf8Error> {
        unsafe {
            Ok(CStr::from_ptr(xm_get_module_name(self.0))
                .to_str()?
                .trim_end_matches(' '))
        }
    }

    /// Get the the name of the tracker in which the module was created.
    pub fn tracker_name(&self) -> Result<&str, Utf8Error> {
        unsafe {
            Ok(CStr::from_ptr(xm_get_tracker_name(self.0))
                .to_str()?
                .trim_end_matches(' '))
        }
    }
}

impl Drop for Context {
    fn drop(&mut self) {
        unsafe { xm_free_context(self.0) }
    }
}

/// Module playback speed.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Speed {
    pub bpm: u16,
    pub tempo: u16,
}

/// Module playback position.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Position {
    pub pattern_index: u8,
    pub pattern: u8,
    pub row: u8,
    pub generated_samples: u64,
}

/// Context creation error.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ContextError {
    #[cfg(feature = "defensive")]
    /// Module sanity error.
    ModuleNotSane,
    /// Error of memory allocation for module.
    MemoryAllocFailed,
    Unknown(i32),
}

impl Display for ContextError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            #[cfg(feature = "defensive")]
            Self::ModuleNotSane => f.write_str("Module not sane!"),
            Self::MemoryAllocFailed => f.write_str("Memory allocation failed!"),
            Self::Unknown(number) => f.write_fmt(format_args!("Unknown error: {}!", number)),
        }
    }
}

/// Error of out of bounds.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum OutOfBoundsError {
    Patterns,
    Instruments,
    Channels,
    Samples,
}

impl Display for OutOfBoundsError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Self::Patterns => "Pattern out of bounds!",
            Self::Instruments => "Instrument out of bounds!",
            Self::Channels => "Channel out of bounds!",
            Self::Samples => "Sample out of bounds!",
        })
    }
}

/// Error for [`Context::frequency_of_channel`] function.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ChannelError {
    OutOfBounds(OutOfBoundsError),
    NotActive,
}

impl Display for ChannelError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::OutOfBounds(error) => Display::fmt(error, f),
            Self::NotActive => f.write_str("Channel is not active!"),
        }
    }
}
