pub type ContextPointer = *mut std::ffi::c_void;

extern "C" {
    pub fn xm_create_context_safe(
        context: *mut ContextPointer,
        moddata: *const i8,
        moddata_length: usize,
        rate: u32,
    ) -> i32;
    pub fn xm_generate_samples(context: ContextPointer, output: *mut f32, numsamples: usize);
    pub fn xm_get_loop_count(context: ContextPointer) -> u8;
    pub fn xm_free_context(context: ContextPointer);
    pub fn xm_set_max_loop_count(context: ContextPointer, loopcnt: u8);
    pub fn xm_get_number_of_channels(ctx: ContextPointer) -> u16;
    pub fn xm_get_module_length(ctx: ContextPointer) -> u16;
    pub fn xm_get_number_of_patterns(ctx: ContextPointer) -> u16;
    pub fn xm_get_number_of_rows(ctx: ContextPointer, pattern: u16) -> u16;
    pub fn xm_get_number_of_instruments(ctx: ContextPointer) -> u16;
    pub fn xm_get_number_of_samples(ctx: ContextPointer, instrument: u16) -> u16;
    pub fn xm_get_panning_of_channel(ctx: ContextPointer, chn: u16) -> f32;
    pub fn xm_get_volume_of_channel(ctx: ContextPointer, chn: u16) -> f32;
    pub fn xm_get_frequency_of_channel(ctx: ContextPointer, chn: u16) -> f32;
    pub fn xm_is_channel_active(ctx: ContextPointer, chn: u16) -> bool;
    pub fn xm_get_playing_speed(ctx: ContextPointer, bpm: *mut u16, tempo: *mut u16);
    pub fn xm_get_position(
        ctx: ContextPointer,
        pattern_index: *mut u8,
        pattern: *mut u8,
        row: *mut u8,
        samples: *mut u64,
    );
    pub fn xm_get_latest_trigger_of_instrument(ctx: ContextPointer, instr: u16) -> u64;
    pub fn xm_get_latest_trigger_of_sample(ctx: ContextPointer, instr: u16, sample: u16) -> u64;
    pub fn xm_get_latest_trigger_of_channel(ctx: ContextPointer, chn: u16) -> u64;
    pub fn xm_mute_channel(ctx: ContextPointer, channel: u16, mute: bool) -> bool;
    pub fn xm_mute_instrument(ctx: ContextPointer, instr: u16, mute: bool) -> bool;
    pub fn xm_seek(ctx: ContextPointer, pot: u8, row: u8, tick: u16);
}

#[cfg(feature = "strings")]
extern "C" {
    pub fn xm_get_module_name(ctx: ContextPointer) -> *const i8;
    pub fn xm_get_tracker_name(ctx: ContextPointer) -> *const i8;
}
