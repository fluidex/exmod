fn main() {
    let mut build = cc::Build::new();

    if std::env::var("PROFILE").unwrap() == "debug" {
        build.define("XM_DEBUG", "1")
    } else {
        build.define("XM_DEBUG", "0")
    };

    #[cfg(feature = "defensive")]
    build.define("XM_DEFENSIVE", "1");
    #[cfg(not(feature = "defensive"))]
    build.define("XM_DEFENSIVE", "0");

    #[cfg(feature = "linear_interpolation")]
    build.define("XM_LINEAR_INTERPOLATION", "1");
    #[cfg(not(feature = "linear_interpolation"))]
    build.define("XM_LINEAR_INTERPOLATION", "0");

    #[cfg(feature = "ramping")]
    build.define("XM_RAMPING", "1");
    #[cfg(feature = "strings")]
    build.define("XM_STRINGS", "1");

    build
        .define("XM_LIBXMIZE_DELTA_SAMPLES", "0")
        .files([
            "libxm/src/xm.c",
            "libxm/src/context.c",
            "libxm/src/load.c",
            "libxm/src/play.c",
        ])
        .include("libxm/include")
        .compile(env!("CARGO_PKG_NAME"));
}
